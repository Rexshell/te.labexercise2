
#include <conio.h>
#include <iostream>
#include <string>

using namespace std;


enum Rank
{
	Two = 2, Three = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9, Ten = 10, J = 11, Q = 12, K = 13, A = 14
};

enum Suit
{
	Hearts, Clubs, Diamonds, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{

	_getch();
	return 0;
}